package com.jeff.mygradle

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.jeff.mygradle.databinding.ActivityMainBinding
import com.jeff.test.MyLib

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tv1.text = BuildConfig.FLAVOR_color
        binding.tv2.text = BuildConfig.FLAVOR_name
        binding.tv3.text = BuildConfig.FlavorName
        binding.tv4.text = packageName
        binding.tv5.text = javaClass.name

        Log.d("Jeff", MyLib().tag)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        Log.d("Jeff", "$newConfig")
    }
}
